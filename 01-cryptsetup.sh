#!/bin/bash

echo "Use this script with care. Whatever device you choose to install to will be completely formatted and any data on it will be lost."

echo "This script prepares a LUKS encrypted partition. It currently assumes atthat a single partition on a single disk will be used for the root Void Linux. If you want to use a mirrored, raidz, etc. configuration etc, you should manually edit this script accordingly. "

echo "Here and throughout, hitting the <return> key accepts the default value shown in [brackets]."

read -p "Enter the device you want to install to [/dev/sda]: " DEVICE
DEVICE=${DEVICE:-/dev/sda}

# determine if device is SSD or not
ROTATIONAL=`cat /sys/block/"${DEVICE#/dev/}"/queue/rotational` 

read -p "Enter the name to assign to the LUKS partition [cryptz]: " LUKSNAME

LUKSNAME=${LUKSNAME:-cryptz}

read -p "You want to install to ${DEVICE}? Are you sure? Proceeding will irrevocably wipe ${DEVICE}. Please type 'yes' in ALLCAPS to proceed: " proceed

if [ ${proceed} == "YES" ]
then
    # erase drive(!)
    wipefs --force --all ${DEVICE}

    # Set MBR
    /sbin/parted --script --align opt ${DEVICE} mklabel msdos
    /sbin/parted --script --align opt ${DEVICE} mkpart pri 1MiB 100%
    /sbin/parted --script --align opt ${DEVICE} set 1 boot on
    /sbin/parted --script --align opt ${DEVICE} p # print

    # Create LUKS container and open/mount it
    cryptsetup luksFormat --type luks1 ${DEVICE}1

    # Open LUKS container
    echo "Retype password to open LUKS partition: "
    cryptsetup luksOpen ${DEVICE}1 ${LUKSNAME}

    # We put this UUID into an env var to reuse later
    CRYPTUUID=`blkid -o export ${DEVICE}1 | grep -E '^UUID='`

    # export LUKSNAME
    echo "export LUKSNAME=$LUKSNAME" > ./importvars.sh
    
    # message to user
    echo "You have now prepared ${DEVICE}1 as a LUKS partition (named ${LUKSNAME}). You may proceed to the next step; execute the following in the terminal:"
    echo ". 02-zpool-creation.sh"
else
    echo "Cancelled."
fi
